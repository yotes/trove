# Trove

A *very* opinionated static site generator written in [Factor](https://factorcode.org) for [my own use](https://den.wrwlf.net). I'd highly advice against using this yourself as it was not written with extensibility or customization in mind, but if you want to anyways:

### Running:

- To output an executable you can use as a regular program; in a Factor listener:

    ```factor
    > "trove" deploy
    ```

    Assuming your Factor installation is located at `/opt/factor`:

    ```sh
    $ /opt/factor/trove/trove scaffold ~/my-site
    $ cd $HOME/my-site
    ...
    $ /opt/factor/trove/trove build
    ...
    ```

- If you're tinkering with the source, you can also run from a listener session, to avoid the waiting times of having Factor compile the libraries over and over on each invocation:

    ```factor
    > USE: trove
    > USE: io.directories
    > "path/to/site" [ [ generate-site ] with-config ] with-directory
    ```

### Scaffolding:

Scaffolding a new site can be done:

- from a Factor listener

    ```factor
    > USE: trove
    > "path/to/site" scaffold-site-at
    A new site has been scaffolded at P" .../path/to/site".
    ```

- from the command line (assuming you deployed Trove into an executable)

    ```sh
    $ path/to/trove scaffold path/to/site
    ```

### Writing:

Posts are written in the [Farkup format](https://docs.factorcode.org/content/vocab-farkup.html), and should include metadata such as title, description and publish date in a YAML-like frontmatter format:

```
title: Test post
description: A description for the post
date: 2022-08-08
---
= Heading =
A paragraph with a [[link||https://factorcode.org]] to the Factor homepage
```

Allowed values are:

- `title`, typed as a string
- `description`, typed as a string
- `date`, typed as a **YYYY-MM-DD** timestamp
- `tags`, typed as a comma-separated array of strings (currently unused)
- `page`, typed as a boolean

### Configuration:

Refer to the generated configuration file when scaffolding (`trove.cfg`) for information on the variables that can be set.