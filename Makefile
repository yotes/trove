FILES = trove.factor templates/templates.factor types/types.factor
FACTOR = /home/user/src/factor/factor

deploy: deploy.factor $(FILES)
	$(FACTOR) -e='"trove" deploy'
